% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/calcul_parametres.R
\name{calcul_beta}
\alias{calcul_beta}
\title{Calculation of beta}
\usage{
calcul_beta(s, pi, P, R, Y, c)
}
\arguments{
\item{s}{(integer) number of hidden states}

\item{pi}{(vector of probas) initial distribution (pi = pi_vrai)}

\item{P}{(list of probas matrix) transition matrix}

\item{R}{(matrix of probas) emission matrix}

\item{Y}{(vector of integers) observations}

\item{c}{(integer) number of the current chain}
}
\value{
= beta
}
\description{
Calculation of beta
}
\examples{
s <- 2
pi <- c(1/2, 1/2)
c <- 1
R <- matrix(rep(1/2, 4), ncol = 2, nrow = 2) #loi d'émission
P <- list(matrix(rep(1/2, 4), ncol = 2, nrow = 2),
                matrix(rep(1/2, 4), ncol = 2, nrow = 2))
Y <- matrix(c(1, 1, 2, 2), nrow = 2)
calcul_beta(s, pi, P, R, Y, c)
}
