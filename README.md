# Package used in the paper Observation Driven HMM

## Package presentation

In this package, we offer different functions allowing you to:

1. Simulate observation trajectories and generate the parameters of an OD-HMM.
2. Estimate the different parameters using the Froward algorithm and the Backward algorithm.
3. Conduct the entire EM algorithm to obtain an estimate of all parameters in several iterations.

## How to install

With R : 

```
# install.packages("remotes") # If not installed. 
remotes::install_git("https://forgemia.inra.fr/hanna.bacave/odhmm.git")
```


## Example of code
Please be careful, as this algorithm uses up a lot of resources when M is large.

```
M <- 10

epsilon <- 0.001 
max_iteration <- 100

# Init
A <- c(1, 2) #observed state space
d <- length(A)
E <- c(1, 2) #hidden state space
s <- length(E)
C <- 5
K <- 10

pi_true <- c(1, 0)
R_true <- matrix(c(0.8, 0.2, 0.2, 0.8), ncol = d, nrow = s)
P_true <-
  list(matrix(c(0.2, 0.8, 0.8, 0.2), ncol = s, nrow = s), matrix(c(0.8, 0.2, 0.2, 0.8), ncol = s, nrow = s))
theta_true <- list("pi" = pi_true,"P" = P_true, "R" = R_true)

obs <- N_simus(C, E, A, M, pi_true, P_true, R_true)
Y <- obs$Y

res <- EM_multichaine_ind(d, s, pi_vraie, P_vraie, R_vraie, Y, epsilon, max_iteration)
```
