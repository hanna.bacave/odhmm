test_that("simulation_Y works", {
  E <- A <- 2
  M <- 10
  pi <- c(1, 0)
  R <- matrix(rep(1/2, 4), ncol = 2, nrow = 2) #loi d'émission
  P <- list(matrix(rep(1/2, 4), ncol = 2, nrow = 2),
  matrix(rep(1/2, 4), ncol = 2, nrow = 2))

  set.seed(123)
  simu <- simulation_Y(E, A, M, pi, P, R)

  expect_identical(
    simu,
    list("Y"= c(1L,1L,2L,1L,2L,2L,1L,1L,2L,1L),
         "Z" = c(1L,2L,1L,1L,1L,1L,1L,2L,2L,2L))
  )
})


test_that("N_simus works", {
  N <- 5
  E <- A <- 2
  M <- 10
  pi <- c(1, 0)
  R <- matrix(rep(1/2, 4), ncol = 2, nrow = 2) #loi d'émission
  P <- list(matrix(rep(1/2, 4), ncol = 2, nrow = 2),
  matrix(rep(1/2, 4), ncol = 2, nrow = 2))

  set.seed(123)
  simus <- N_simus(N, E, A, M, pi, P, R)

  expect_identical(
    simus,
    list("Y" = matrix(c(1L,1L,2L,2L,1L,1L,1L,2L,2L,1L,
                        2L,1L,2L,2L,2L,1L,1L,2L,1L,1L,
                        2L,2L,1L,2L,2L,2L,1L,2L,1L,1L,
                        1L,1L,2L,2L,1L,1L,2L,2L,2L,2L,
                        2L,2L,1L,1L,2L,1L,2L,2L,2L,1L), nrow=5),
         "Z" = matrix(c(1L,1L,1L,1L,1L,2L,1L,2L,2L,2L,
                        1L,1L,2L,1L,2L,1L,1L,2L,1L,1L,
                        1L,2L,2L,1L,1L,1L,1L,2L,1L,2L,
                        1L,1L,1L,1L,2L,2L,2L,1L,2L,2L,
                        2L,1L,2L,2L,1L,2L,2L,1L,2L,2L), nrow=5))
  )
})
